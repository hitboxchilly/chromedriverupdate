import argparse
import configparser
import io
import json
import logging.config
import os
import shutil
import shutil
import subprocess
import sys
import textwrap
import urllib.request

from pprint import pprint
from zipfile import ZipFile

try:
    import winreg
except ModuleNotFoundError:
    winreg = None

PROGNAME = 'chromedriverupdate'
LOGGING_SECTIONS = ['loggers', 'handlers', 'formatters']

CHROMEDRIVER_DATA_URL = (
    'https://googlechromelabs.github.io'
    '/chrome-for-testing'
    '/known-good-versions-with-downloads.json'
)

PLATFORMS = ['linux64', 'win32', 'win64']

RUNTIME_INJECTIONS = [
    # key, global func name
    (
        'chromedriver_dir',
        'get_chromedriver_dir',
    ),
    (
        'installed_browser_version',
        'get_installed_browser_version',
    ),
    (
        'installed_chromedriver_version',
        'get_installed_chromedriver_version',
    ),
]

class ChromeDriverUpdateError(Exception):
    """
    Chrome driver error updating.
    """

def configure_logging(cp):
    if all(cp.has_section(section) for section in LOGGING_SECTIONS):
        logging.config.fileConfig(cp)
    else:
        # fallback to basic config if no config given,
        # or it has insufficient sections
        logging.basicConfig(level=logging.INFO)

def get_chromedriver_url(version, platform):
    """
    Download Google's JSON and find the url to download a chromedriver
    executable for a version and platform.
    """
    with urllib.request.urlopen(CHROMEDRIVER_DATA_URL) as response:
        data = json.load(response)
    # find version...
    for version_data in data['versions']:
        if version_data['version'] == version:
            break
    else:
        # ...or raise
        raise ChromeDriverUpdateError('Unable to find version.')
    # find platform and return url...
    for download_data in version_data['downloads']['chromedriver']:
        if download_data['platform'] == platform:
            return download_data['url']
    # ...or raise
    raise ChromeDriverUpdateError('Unable to find platform.')

def get_windows_chrome_version():
    hkey = winreg.OpenKey(
        winreg.HKEY_CURRENT_USER,
        'Software\Google\Chrome\BLBeacon',
        0,
        winreg.KEY_READ)
    versionstring, regtype = winreg.QueryValueEx(hkey, 'version')
    return versionstring

def get_chromium_version():
    result = subprocess.run(['chromium', '--version'],
        capture_output=True, check=True, text=True
    )
    return result.stdout.split()[1]

def get_installed_browser_version():
    """
    Return the version of installed Chrome.
    """
    if os.name == 'posix':
        return get_chromium_version()
    elif os.name == 'nt':
        return get_windows_chrome_version()
    else:
        raise ChromeDriverUpdateError('Operating system not supported.')

def get_installed_chromedriver_version():
    """
    Return the version of install chromedriver
    """
    result = subprocess.run(
        ['chromedriver', '--version'],
        capture_output=True, check=True, text=True)
    versionstring = result.stdout.split()[1]
    return versionstring

def comment(string, comment_prefix='# ', separator='\n', subsequent_indent=None):
    if subsequent_indent is None:
        subsequent_indent = comment_prefix
    lines = textwrap.wrap(
        comment_prefix + string,
        subsequent_indent = subsequent_indent,
    )
    return separator.join(lines)

def prompt_string(key, choices=None, default=None):
    parts = [key]
    if choices is not None:
        parts.append(
            f"{{{', '.join(map(repr, choices))}}}",
        )
    if default is not None:
        parts.append(f"[{default!r}]")
    return ' '.join(parts) + ': '

def input_string(prompt, default=None, validate=None, errormsg='Invalid'):
    if default is None:
        default = ''
    if validate is None:
        validate = lambda answer: True
    while True:
        answer = input(prompt)
        if not answer:
            answer = default
        if validate(answer):
            return answer
        print(errormsg)

def input_choose(prompt, choices, default=None):
    def validate(answer):
        return answer in choices
    return input_string(prompt, default, validate, 'Invalid choice')

def platform_prompt(default=None):
    prompt = prompt_string('platform', PLATFORMS, default=default)
    platform = input_choose(prompt, PLATFORMS, default=default)
    return platform

def zip_member_prompt(default=None):
    if default is None:
        default = 'chromedriver-%(platform)s/chromedriver.exe'
    prompt = prompt_string('zip_member', default=default)
    return input_string(prompt, default)

def dest_prompt(default=None):
    if default is None:
        # a default that uses the runtime value chromedriver_dir
        default = '{chromedriver_dir}/chromedriver.exe'
    prompt = prompt_string('dest', default=default)
    return input_string(prompt, default)

def init_config(options, file=None):
    """
    Interactively build a config file.
    """
    options['platform'] = platform_prompt(options.get('platform'))
    options['zip_member'] = zip_member_prompt()
    options['dest'] = dest_prompt()

    lines = [
        # what this is blurb
        comment(
            'Configuration for updating the chromedriver executable.\n'
        ),
        # section name
        f'[{PROGNAME}]',
        # notes about interpolation and substitution
        '',
        comment(
            'Interpolation and String Substitution',
        ),
        comment(
            'There are two stages of interpolation and string substition.'
        ),
        comment(
            # first stage comment
            '1. Normal ConfigParser interpolation using old-style formatting'
            ' for values from this config.',
            subsequent_indent = '#    ',
        ),
        comment(
            # second stage comment
            '2. In the second stage, runtime values are gathered and the'
            ' configuration values are given a chance to use them. These use'
            ' new-style formatting string substitution.',
            subsequent_indent = '#    ',
        ),
        '',
        comment(
            'Runtime injection variable names:'
        ),
        comment(
            'chromedriver_dir:'
        ),
        comment(
            '    The directory where the chromedriver executable was'
            ' found. Probably what you want to use in the `dest` key.',
            subsequent_indent = '#     ',
        ),
        '',
        comment(
            'installed_browser_version:'
        ),
        comment(
            '    The full version string of the browser.'
        ),
        '',
        comment(
            'installed_chromedriver_version:'
        ),
        comment(
            '    The full version string of the chromedriver executable'
            ' found on the path.',
            subsequent_indent = '#     ',
        ),
        # key/values start with platform
        '',
        comment(
            'A string used to find the chromedriver executable to download.'
        ),
        comment(
            f'Valid values: [{", ".join(map(repr, PLATFORMS))}]'
        ),
        f'platform = {options["platform"]}',
        '',
        comment(
            'The path inside the remote zip file to the'
            ' chromedriver executable.'
        ),
        f'zip_member = {options["zip_member"]}',
        '',
        comment(
            'Where to write the downloaded chromedriver executable file.'
        ),
        f'dest = {options["dest"]}',
    ]
    ini_string = '\n'.join(lines)
    print(ini_string, file=file)

def parse_args(argv):
    """
    Parse command line arguments.
    """
    parser = argparse.ArgumentParser(
        description = run.__doc__,
        prog = PROGNAME,
    )
    parser.add_argument(
        'config',
        nargs = '*',
    )

    # command line options override config files
    parser.add_argument(
        '--platform',
        choices = PLATFORMS,
        help = "Google's platform specification to download and install.",
    )
    parser.add_argument(
        '--zip-member',
        metavar = 'NAME',
        help =
            'Full path to the member in remote zip file for the'
            ' chromedriver executable.',
    )
    parser.add_argument(
        '--dest',
        help = 'Full path including file name to write executable.'
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--force',
        action = 'store_true',
        help = 'Update even if versions are same.'
    )
    group.add_argument(
        '--dump',
        action = 'store_true',
        help = 'Dump configuration after parsing and exit.'
    )
    group.add_argument(
        '--init',
        nargs = '?', # write to given file or fallback to stdout
        metavar = 'INI_FILE',
        # option given without argument, fallback to stdout:
        const = sys.stdout,
        type = argparse.FileType('w'),
        help = 'Write example config with documentation.'
    )
    args = parser.parse_args(argv)
    return args

def get_chromedriver_dir():
    return os.path.dirname(shutil.which('chromedriver'))

def inject_runtime_values(options):
    for key, funcname in RUNTIME_INJECTIONS:
        func = globals()[funcname]
        options[key] = func()

def inject_runtime_format(options):
    # give config change to use runtime values
    def format_runtime(val):
        if isinstance(val, str):
            return val.format(**options)
        return val

    for key, val in options.items():
        options[key] = format_runtime(val)

def parse_options(args):
    """
    Parse command line arguments into options that must exist.
    """
    config_files = args.config
    del args.config
    cp = configparser.ConfigParser()
    cp.read(config_files)
    configure_logging(cp)

    options = dict(cp[PROGNAME]) if (PROGNAME in cp) else {}
    options.update({k:v for k,v in vars(args).items() if v})
    # ensure boolean keys are present
    options['force'] = args.force
    options['dump'] = args.dump
    options['init'] = args.init

    inject_runtime_values(options)
    inject_runtime_format(options)

    return options

def validate_options(options):
    if options['platform'] not in PLATFORMS:
        raise ChromeDriverUpdateError('Invalid platform.')

def needs_update(options):
    force_update = options['force']

    installed_browser_version = options['installed_browser_version']
    installed_chromedriver_version = options['installed_chromedriver_version']

    # determine if update needed or if forced
    is_same_version = (
        installed_chromedriver_version == installed_browser_version
    )
    return force_update or not is_same_version

def run(options):
    """
    Update chromedriver exectuable.
    """
    # raise for necessary keys and get for convenience
    platform = options['platform']
    zip_member = options['zip_member']
    dest = options['dest']
    force_update = options['force']
    installed_browser_version = options['installed_browser_version']
    installed_chromedriver_version = options['installed_chromedriver_version']

    # NOTE
    # - may have to use the major version number
    # download zip from url...
    chromedriver_url = get_chromedriver_url(
        installed_browser_version,
        platform,
    )
    with (
        urllib.request.urlopen(chromedriver_url) as response,
        io.BytesIO() as zip_fp,
    ):
        # ...stream/download into memory file that ZipFile accepts
        zip_fp.writelines(response)
        zip_fp.seek(0)
        with ZipFile(zip_fp) as zipfile:
            # open zip, read member and write to file
            with (
                zipfile.open(zip_member) as chromedriver_src,
                open(dest, 'wb') as dest_file,
            ):
                dest_file.writelines(chromedriver_src)

def main(argv=None):
    """
    Wrap run func to capture exceptions.
    """
    args = parse_args(argv)
    options = parse_options(args)

    # if dump, show pretty final options
    if options['dump']:
        del options['dump']
        del options['init']
        return pprint(options)

    # if init(ialize config) begin interactive config build
    if options['init']:
        init_file = options['init']
        del options['init']
        return init_config(options, file=init_file)

    validate_options(options)

    # if nothing to do, stop
    if not needs_update(options):
        return

    logger = logging.getLogger(PROGNAME)
    try:
        return run(options)
    except:
        logger.exception('An exception occurred')

if __name__ == '__main__':
    main()
